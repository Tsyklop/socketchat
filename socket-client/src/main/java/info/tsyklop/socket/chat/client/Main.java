package info.tsyklop.socket.chat.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws IOException {

        try (BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in))) {

            LOGGER.info("Enter server ip: ");

            String ip = consoleReader.readLine().trim();

            LOGGER.info("Enter server port:");
            Integer port = Integer.valueOf(consoleReader.readLine().trim());

            new Client(ip, port, consoleReader).start();

        } catch (NumberFormatException e) {
            LOGGER.error("Port entered incorrect");
        }

    }

}
