package info.tsyklop.socket.chat.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;
import java.util.Objects;

public class Client {

    private boolean stopped;

    private final Socket socket;

    private final BufferedReader consoleReader;

    private final BufferedReader bufferedReader;
    private final BufferedWriter bufferedWriter;

    private final static Logger LOGGER = LoggerFactory.getLogger(Client.class);

    public Client(String ip, Integer port, BufferedReader consoleReader) throws IOException {
        this.stopped = false;
        this.socket = new Socket(ip, port);
        this.consoleReader = consoleReader;
        this.bufferedReader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
        this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream()));
    }

    public void start() {

        new Thread(new MessagePrinter(this)).start();

        try {

            while (!this.stopped) {

                String message = this.consoleReader.readLine();

                sendMessage(message);

                if (message.equalsIgnoreCase("quit")
                        || message.equalsIgnoreCase("exit")
                        || message.equalsIgnoreCase("disconnect")) {
                    break;
                }

            }

        } catch (IOException e) {
            LOGGER.error("CLIENT ERROR", e);
        } finally {
            stop();
        }

    }

    public void stop() {

        try {
            this.bufferedReader.close();
        } catch (IOException ignored) {
        }

        try {
            this.bufferedWriter.close();
        } catch (IOException ignored) {
        }

        try {
            this.socket.close();
        } catch (IOException ignored) {
        }

        this.stopped = true;

    }

    public boolean isLive() {
        return !this.stopped && this.socket.isConnected();
    }

    public BufferedReader getBufferedReader() {
        return this.bufferedReader;
    }

    public BufferedWriter getBufferedWriter() {
        return this.bufferedWriter;
    }

    public void sendMessage(String message) throws IOException {
        if (Objects.nonNull(message) && !message.isEmpty()) {
            this.bufferedWriter.write(message + "\n");
            this.bufferedWriter.flush();
        } else {
            LOGGER.info("Please, enter message");
        }
    }

    private static class MessagePrinter implements Runnable {

        private final Client client;

        private final static Logger LOGGER = LoggerFactory.getLogger(MessagePrinter.class);

        private MessagePrinter(Client client) {
            this.client = client;
        }

        @Override
        public void run() {

            try {

                while (this.client.isLive()) {

                    String message = this.client.getBufferedReader().readLine();

                    if (Objects.nonNull(message)) {
                        LOGGER.info(message);
                    }

                }

            } catch (IOException e) {
                //LOGGER.error("MESSAGES PRINTER ERROR:", e);
                this.client.stop();
            }

        }

    }

}
