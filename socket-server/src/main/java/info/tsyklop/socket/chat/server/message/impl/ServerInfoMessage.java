package info.tsyklop.socket.chat.server.message.impl;

import info.tsyklop.socket.chat.server.message.AbstractMessage;
import info.tsyklop.socket.chat.server.message.Message;
import info.tsyklop.socket.chat.server.model.MessageType;

public class ServerInfoMessage extends AbstractMessage {

    protected ServerInfoMessage(String message) {
        super(null, MessageType.SERVER_INFO, message);
    }

    @Override
    public String toString() {
        return String.format("<Server>: %s", getMessage());
    }

    public static Message create(String message) {
        return new ServerInfoMessage(message);
    }

}
