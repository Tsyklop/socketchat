package info.tsyklop.socket.chat.server.processor.impl;

import info.tsyklop.socket.chat.server.client.ClientThread;
import info.tsyklop.socket.chat.server.message.Message;
import info.tsyklop.socket.chat.server.message.impl.CommandMessage;
import info.tsyklop.socket.chat.server.message.impl.ServerInfoMessage;
import info.tsyklop.socket.chat.server.model.CommandType;
import info.tsyklop.socket.chat.server.processor.MessageProcessor;
import info.tsyklop.socket.chat.server.repository.ClientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.stream.Collectors;

public class CommandMessageProcessor implements MessageProcessor<CommandMessage> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandMessageProcessor.class);

    @Override
    public boolean isSupport(Message message) {
        return CommandMessage.class == message.getClass();
    }

    @Override
    public synchronized void process(ClientThread clientThread, CommandMessage message) {

        try {

            switch (message.getCommandType()) {
                case HELP:
                    clientThread.sendMessage(ServerInfoMessage.create(CommandType.toHelp()));
                    break;
                case USERS_LIST:
                    clientThread.sendMessage(ServerInfoMessage.create(String.format("Connected users: %s", ClientRepository.getConnectedUsersAsString())));
                    break;
                case IGNORE_USER:

                    String[] ignoreUsers = message.getMessage().split(" ");

                    if(ignoreUsers.length > 0) {

                        for(String clientName: ignoreUsers) {
                            clientThread.toIgnore(clientName);
                        }

                    }

                    break;
            }

        } catch (IOException e) {
            LOGGER.error("COMMAND MESSAGE PROCESSOR ERROR", e);
        }

    }

}
