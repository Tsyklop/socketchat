package info.tsyklop.socket.chat.server.message.parser.impl;

import info.tsyklop.socket.chat.server.client.ClientThread;
import info.tsyklop.socket.chat.server.exception.MessageException;
import info.tsyklop.socket.chat.server.message.Message;
import info.tsyklop.socket.chat.server.message.impl.CommandMessage;
import info.tsyklop.socket.chat.server.message.parser.AbstractParser;
import info.tsyklop.socket.chat.server.message.parser.MessageParser;
import info.tsyklop.socket.chat.server.model.CommandType;

import java.util.Objects;

public class CommandMessageParser extends AbstractParser {

    @Override
    public boolean test(String message) {
        return isCommandMessage(message);
    }

    @Override
    public Message parse(ClientThread source, String message) throws MessageException {

        String[] messageParts = message.split(" ", 2);

        if (messageParts.length >= 1) {

            CommandType commandType = CommandType.parse(messageParts[0]);

            if (!commandType.isUnknown()) {
                return new CommandMessage(source, messageParts.length > 1 ? messageParts[1].trim() : "", commandType);
            } else {
                throw new MessageException("Unknown command");
            }

        } else {
            throw new MessageException("Sending message error");
        }

        //return new UnknownMessage();

    }

}
