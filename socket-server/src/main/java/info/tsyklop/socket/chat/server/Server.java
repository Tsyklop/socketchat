package info.tsyklop.socket.chat.server;

import info.tsyklop.socket.chat.server.client.ClientThread;
import info.tsyklop.socket.chat.server.config.Config;
import info.tsyklop.socket.chat.server.repository.ClientRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executors;

public class Server {

    private final Config config;

    private final ServerSocket serverSocket;

    private final static Logger LOGGER = LoggerFactory.getLogger(Server.class);

    public Server(Config config) throws IOException {
        this.config = config;
        this.serverSocket = new ServerSocket(config.getPort());
    }

    public void start() {

        LOGGER.info("SERVER STARTED. WAITING FOR CONNECTIONS...");

        while(true) {

            try {

                Socket clientSocket = this.serverSocket.accept();

                LOGGER.info("CATCH NEW CONNECTION...");

                ClientThread clientThread = new ClientThread(clientSocket);

                Thread thread = new Thread(clientThread);
                thread.setDaemon(true);
                thread.setName(String.format("Client-Thread-%s", RandomStringUtils.randomAlphanumeric(30)));
                thread.start();

                ClientRepository.addNonConnectedClient(clientThread);

                LOGGER.info("UNKNOWN CLIENT CONNECTED");

            } catch (IOException e) {
                LOGGER.error("SERVER ERROR", e);
            }

        }

    }

}
