package info.tsyklop.socket.chat.server.processor;

import info.tsyklop.socket.chat.server.client.ClientThread;
import info.tsyklop.socket.chat.server.message.Message;

public interface MessageProcessor<T extends Message> {

    boolean isSupport(Message message);

    void process(ClientThread clientThread, T message);

}
