package info.tsyklop.socket.chat.server.processor.impl;

import info.tsyklop.socket.chat.server.client.ClientThread;
import info.tsyklop.socket.chat.server.message.Message;
import info.tsyklop.socket.chat.server.message.impl.BroadcastMessage;
import info.tsyklop.socket.chat.server.message.impl.CommandMessage;
import info.tsyklop.socket.chat.server.processor.MessageProcessor;

public class BroadcastMessageProcessor implements MessageProcessor<BroadcastMessage> {

    @Override
    public boolean isSupport(Message message) {
        return BroadcastMessage.class == message.getClass();
    }

    @Override
    public synchronized void process(ClientThread clientThread, BroadcastMessage message) {
        clientThread.broadcastMessage(message);
    }

}
