package info.tsyklop.socket.chat.server.processor.impl;

import info.tsyklop.socket.chat.server.client.ClientThread;
import info.tsyklop.socket.chat.server.message.Message;
import info.tsyklop.socket.chat.server.message.impl.BroadcastMessage;
import info.tsyklop.socket.chat.server.message.impl.DisconnectMessage;
import info.tsyklop.socket.chat.server.processor.MessageProcessor;

public class DisconnectMessageProcessor implements MessageProcessor<DisconnectMessage> {

    @Override
    public boolean isSupport(Message message) {
        return DisconnectMessage.class == message.getClass();
    }

    @Override
    public synchronized void process(ClientThread clientThread, DisconnectMessage message) {
        clientThread.broadcastMessage(BroadcastMessage.create(message.toString()));
        clientThread.disconnect();
    }

}
