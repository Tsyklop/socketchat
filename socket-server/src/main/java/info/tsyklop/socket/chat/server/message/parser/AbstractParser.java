package info.tsyklop.socket.chat.server.message.parser;

import java.util.Objects;

public abstract class AbstractParser implements MessageParser {

    protected boolean isCommandMessage(String message) {
        return Objects.nonNull(message) && !message.isEmpty() && message.startsWith("/");
    }

    protected boolean isPersonalMessage(String message) {
        return Objects.nonNull(message) && !message.isEmpty() && message.startsWith("@");
    }

    protected boolean isDisconnectMessage(String message) {
        return Objects.nonNull(message) && !message.isEmpty() &&
                (message.equalsIgnoreCase("quit")
                        || message.equalsIgnoreCase("exit")
                        || message.equalsIgnoreCase("disconnect"));
    }

}
