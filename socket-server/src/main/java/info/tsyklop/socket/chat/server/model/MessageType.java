package info.tsyklop.socket.chat.server.model;

public enum MessageType {
    ERROR, BROADCAST, PERSONAL, COMMAND, SERVER_INFO, DISCONENCT, UNKNOWN;
}
