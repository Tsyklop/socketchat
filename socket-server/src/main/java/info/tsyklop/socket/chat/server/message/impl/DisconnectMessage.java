package info.tsyklop.socket.chat.server.message.impl;

import info.tsyklop.socket.chat.server.client.ClientThread;
import info.tsyklop.socket.chat.server.message.AbstractMessage;
import info.tsyklop.socket.chat.server.model.MessageType;

public class DisconnectMessage extends AbstractMessage {

    public DisconnectMessage(ClientThread owner, String message) {
        super(owner, MessageType.DISCONENCT, message);
    }

    @Override
    public String toString() {
        return String.format("Client %s disconnected", getOwner().getName());
    }

}
