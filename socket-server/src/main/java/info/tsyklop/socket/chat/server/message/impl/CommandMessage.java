package info.tsyklop.socket.chat.server.message.impl;

import info.tsyklop.socket.chat.server.client.ClientThread;
import info.tsyklop.socket.chat.server.message.AbstractMessage;
import info.tsyklop.socket.chat.server.model.CommandType;
import info.tsyklop.socket.chat.server.model.MessageType;

public class CommandMessage extends AbstractMessage {

    private final CommandType commandType;

    public CommandMessage(ClientThread owner, String message, CommandType commandType) {
        super(owner, MessageType.COMMAND, message);
        this.commandType = commandType;
    }

    public CommandType getCommandType() {
        return this.commandType;
    }

}
