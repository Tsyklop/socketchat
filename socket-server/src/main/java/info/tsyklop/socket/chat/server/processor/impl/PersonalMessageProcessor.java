package info.tsyklop.socket.chat.server.processor.impl;

import info.tsyklop.socket.chat.server.client.ClientThread;
import info.tsyklop.socket.chat.server.message.Message;
import info.tsyklop.socket.chat.server.message.impl.ServerErrorMessage;
import info.tsyklop.socket.chat.server.message.impl.PersonalMessage;
import info.tsyklop.socket.chat.server.processor.MessageProcessor;
import info.tsyklop.socket.chat.server.repository.ClientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Objects;

public class PersonalMessageProcessor implements MessageProcessor<PersonalMessage> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonalMessageProcessor.class);

    @Override
    public boolean isSupport(Message message) {
        return PersonalMessage.class == message.getClass();
    }

    @Override
    public void process(ClientThread clientThread, PersonalMessage message) {

        try {

            ClientThread existedClientThread = ClientRepository.findConnectedClientByName(message.getTo());

            if (Objects.nonNull(existedClientThread)) {
                existedClientThread.sendMessage(message);
                clientThread.sendMessage(message);
            } else {
                clientThread.sendMessage(ServerErrorMessage.create(String.format("Client with name %s not connected", message.getTo())));
            }

        } catch (IOException e) {

            LOGGER.error("PERSONAL MESSAGE PROCESSOR ERROR", e);

            try {
                clientThread.sendMessage(ServerErrorMessage.create("Error sending personal message"));
            } catch (IOException ignored) {}

        }

    }

}
