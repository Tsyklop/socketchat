package info.tsyklop.socket.chat.server.client;

import info.tsyklop.socket.chat.server.message.Message;
import info.tsyklop.socket.chat.server.message.impl.BroadcastMessage;
import info.tsyklop.socket.chat.server.message.impl.ServerErrorMessage;
import info.tsyklop.socket.chat.server.message.impl.ServerInfoMessage;
import info.tsyklop.socket.chat.server.processor.MessageProcessor;
import info.tsyklop.socket.chat.server.processor.impl.BroadcastMessageProcessor;
import info.tsyklop.socket.chat.server.processor.impl.CommandMessageProcessor;
import info.tsyklop.socket.chat.server.processor.impl.DisconnectMessageProcessor;
import info.tsyklop.socket.chat.server.processor.impl.PersonalMessageProcessor;
import info.tsyklop.socket.chat.server.repository.ClientRepository;
import info.tsyklop.socket.chat.server.resolver.MessageResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

public class ClientThread implements Runnable {

    private String name;

    private final Socket socket;

    private final BufferedReader reader;
    private final BufferedWriter writer;

    /*private final DataInputStream reader;
    private final DataOutputStream writer;*/

    private final AtomicBoolean stopped;

    private final List<String> ignoredClients = new ArrayList<>();

    private static final MessageResolver MESSAGE_RESOLVER = new MessageResolver();

    private static final List<MessageProcessor> MESSAGE_PROCESSORS = new ArrayList<>();

    private final static Logger LOGGER = LoggerFactory.getLogger(ClientThread.class);

    static {
        MESSAGE_PROCESSORS.add(new CommandMessageProcessor());
        MESSAGE_PROCESSORS.add(new PersonalMessageProcessor());
        MESSAGE_PROCESSORS.add(new BroadcastMessageProcessor());
        MESSAGE_PROCESSORS.add(new DisconnectMessageProcessor());
    }

    public ClientThread(Socket socket) throws IOException {
        Objects.requireNonNull(socket, "Client socket cannot be null");
        this.socket = socket;
        this.reader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
        this.writer = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream()));
        /*this.reader = new DataInputStream(this.socket.getInputStream());
        this.writer = new DataOutputStream(this.socket.getOutputStream());*/
        this.stopped = new AtomicBoolean(false);
    }

    public String getName() {
        return this.name;
    }

    @Override
    public void run() {

        try {

            authenticateUser();

            while (!this.stopped.get()) {

                String message = this.reader.readLine();

                if (Objects.nonNull(message)) {

                    Message parsedMessage = MESSAGE_RESOLVER.resolve(this, message);

                    for(MessageProcessor messageProcessor: MESSAGE_PROCESSORS) {
                        if(messageProcessor.isSupport(parsedMessage)) {
                            messageProcessor.process(this, parsedMessage);
                        }
                    }

                    /*if (isDisconnectMessage(message)) {
                        broadcastMessage(String.format("%s disconnected", this.name));
                        break;
                    } else if (isShowUsersMessage(message)) {
                        sendMessage(ClientRepository.getConnectedUsers().stream().map(s -> "@" + s).collect(Collectors.joining(",")));
                    } else if (message.startsWith("@")) {

                        String[] messageParts = message.replace("@", "").split(" ", 2);

                        if (messageParts.length >= 2) {

                            ClientThread clientThread = ClientRepository.findConnectedClientByName(messageParts[0]);

                            if (Objects.nonNull(clientThread)) {
                                clientThread.sendMessage(messageParts[1]);
                            } else {
                                sendMessage(String.format("Client with name %s not connected", messageParts[0]));
                            }

                        } else {
                            sendMessage("Sending message error");
                        }

                    } else {
                        //broadcastMessage(String.format("<%s> - %s", this.name, message));
                    }*/

                }

            }

        } catch (IOException e) {
            LOGGER.error("CLIENT THREAD ERROR", e);
        } finally {

            disconnect();

            ClientRepository.removeClient(this.name, this);

            LOGGER.info(String.format("CLIENT %s DISCONNECTED", this.name));

        }

    }

    public synchronized void toIgnore(String clientName) {
        this.ignoredClients.add(clientName);
    }

    public synchronized void sendMessage(Message message) throws IOException {
        try {
            this.writer.write(Objects.requireNonNull(message, "Message cannot be null").toString() + "\n");
            this.writer.flush();
        } catch (IOException e) {
            LOGGER.error("CLIENT THREAD WRITER ERROR", e);
            throw e;
        }
    }

    public synchronized void broadcastMessage(BroadcastMessage broadcastMessage) {
        for (ClientThread clientThread : ClientRepository.getConnectedClients()) {
            if(!this.ignoredClients.contains(clientThread.getName())) {
                try {
                    clientThread.sendMessage(broadcastMessage);
                } catch (IOException ignored) {
                }
            }
        }
    }

    public synchronized void disconnect() {

        try {
            this.reader.close();
        } catch (IOException ignored) {
        }

        try {
            this.writer.close();
        } catch (IOException ignored) {
        }

        try {
            this.socket.close();
        } catch (IOException ignored) {
        }

        this.stopped.set(true);

    }

    private void authenticateUser() throws IOException {

        while (true) {

            sendMessage(ServerInfoMessage.create("Please, enter your name:"));

            String name = this.reader.readLine().trim();

            if (isDisconnectMessage(name)) {
                sendMessage(ServerInfoMessage.create("Bye!"));
                break;
            } else if (name.indexOf('@') == -1) {

                if (!ClientRepository.containsClientByName(name)) {

                    this.name = name;

                    ClientRepository.addConnectedClient(name, this);

                    broadcastMessage(BroadcastMessage.create(String.format("Welcome %s to chat!", name)));

                    LOGGER.info(String.format("CLIENT %s AUTHENTICATED", name));

                    break;

                } else {
                    sendMessage(ServerErrorMessage.create(String.format("Client with name %s already connected", name)));
                }

            } else {
                sendMessage(ServerErrorMessage.create("The name should not contain '@' character."));
            }

        }

    }

    private boolean isDisconnectMessage(String message) {
        return message.equalsIgnoreCase("quit")
                || message.equalsIgnoreCase("disconnect");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientThread that = (ClientThread) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

}
