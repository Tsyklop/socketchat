package info.tsyklop.socket.chat.server.message.impl;

import info.tsyklop.socket.chat.server.message.AbstractMessage;
import info.tsyklop.socket.chat.server.model.MessageType;

public class UnknownMessage extends AbstractMessage {
    public UnknownMessage() {
        super(null, MessageType.UNKNOWN, "Unknown message");
    }
}
