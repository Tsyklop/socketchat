package info.tsyklop.socket.chat.server.message.impl;

import info.tsyklop.socket.chat.server.client.ClientThread;
import info.tsyklop.socket.chat.server.message.AbstractMessage;
import info.tsyklop.socket.chat.server.model.MessageType;

public class PersonalMessage extends AbstractMessage {

    private final String to;

    public PersonalMessage(ClientThread owner, String message, String to) {
        super(owner, MessageType.PERSONAL, message);
        this.to = to;
    }

    public String getTo() {
        return this.to;
    }

    @Override
    public String toString() {
        return String.format("[PERSONAL]<%s -> %s>: %s", getOwner().getName(), getTo(), getMessage());
    }

}
