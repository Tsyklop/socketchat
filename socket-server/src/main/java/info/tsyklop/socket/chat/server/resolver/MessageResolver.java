package info.tsyklop.socket.chat.server.resolver;

import info.tsyklop.socket.chat.server.client.ClientThread;
import info.tsyklop.socket.chat.server.exception.MessageException;
import info.tsyklop.socket.chat.server.message.Message;
import info.tsyklop.socket.chat.server.message.impl.ServerErrorMessage;
import info.tsyklop.socket.chat.server.message.impl.UnknownMessage;
import info.tsyklop.socket.chat.server.message.parser.MessageParser;
import info.tsyklop.socket.chat.server.message.parser.impl.BroadcastMessageParser;
import info.tsyklop.socket.chat.server.message.parser.impl.CommandMessageParser;
import info.tsyklop.socket.chat.server.message.parser.impl.DisconnectMessageParser;
import info.tsyklop.socket.chat.server.message.parser.impl.PersonalMessageParser;

import java.util.ArrayList;
import java.util.List;

public class MessageResolver {

    private final List<MessageParser> parsers = new ArrayList<>();

    public MessageResolver() {
        this.parsers.add(new CommandMessageParser());
        this.parsers.add(new PersonalMessageParser());
        this.parsers.add(new BroadcastMessageParser());
        this.parsers.add(new DisconnectMessageParser());
    }

    public synchronized Message resolve(ClientThread source, String message) {

        try {
            for(MessageParser parser: parsers) {
                if(parser.test(message)) {
                    return parser.parse(source, message);
                }
            }
        } catch (MessageException e) {
            return new ServerErrorMessage(e.getMessage());
        }

        return new UnknownMessage();

    }

}
