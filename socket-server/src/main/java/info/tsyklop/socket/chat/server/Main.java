package info.tsyklop.socket.chat.server;

import info.tsyklop.socket.chat.server.config.Config;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        Config config = new Config(args);
        new Server(config).start();
    }

}
