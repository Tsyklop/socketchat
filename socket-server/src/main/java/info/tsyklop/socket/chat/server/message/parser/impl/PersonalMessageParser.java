package info.tsyklop.socket.chat.server.message.parser.impl;

import info.tsyklop.socket.chat.server.client.ClientThread;
import info.tsyklop.socket.chat.server.exception.MessageException;
import info.tsyklop.socket.chat.server.message.Message;
import info.tsyklop.socket.chat.server.message.impl.PersonalMessage;
import info.tsyklop.socket.chat.server.message.parser.AbstractParser;
import info.tsyklop.socket.chat.server.message.parser.MessageParser;
import info.tsyklop.socket.chat.server.repository.ClientRepository;

import java.util.Objects;

public class PersonalMessageParser extends AbstractParser {

    @Override
    public boolean test(String message) {
        return isPersonalMessage(message);
    }

    @Override
    public Message parse(ClientThread source, String message) throws MessageException {

        String[] messageParts = message.replace("@", "").split(" ", 2);

        if (messageParts.length >= 2) {

            ClientThread clientThread = ClientRepository.findConnectedClientByName(messageParts[0]);

            if (Objects.nonNull(clientThread)) {
                return new PersonalMessage(source, messageParts[1], messageParts[0]);
            } else {
                throw new MessageException(String.format("Client with name %s not connected", messageParts[0]));
            }

        } else {
            throw new MessageException("Sending message error");
        }

    }

}
