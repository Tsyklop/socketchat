package info.tsyklop.socket.chat.server.message.parser.impl;

import info.tsyklop.socket.chat.server.client.ClientThread;
import info.tsyklop.socket.chat.server.exception.MessageException;
import info.tsyklop.socket.chat.server.message.Message;
import info.tsyklop.socket.chat.server.message.impl.DisconnectMessage;
import info.tsyklop.socket.chat.server.message.parser.AbstractParser;
import info.tsyklop.socket.chat.server.message.parser.MessageParser;

public class DisconnectMessageParser extends AbstractParser {

    @Override
    public boolean test(String message) {
        return isDisconnectMessage(message);
    }

    @Override
    public Message parse(ClientThread source, String message) throws MessageException {
        return new DisconnectMessage(source, message);
    }

}
