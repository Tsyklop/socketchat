package info.tsyklop.socket.chat.server.model;

import java.util.Arrays;
import java.util.stream.Collectors;

public enum CommandType {

    HELP("/help", "Show commands list"),
    USERS_LIST("/users", "Show all connected users"),
    IGNORE_USER("/ignore", "Ignore specific user. Example: '/ignore User1' or '/ignore User1 User2'"),
    UNKNOWN("-", "-", false);

    private final String command;

    private final String description;

    private final boolean showInHelp;

    CommandType(String command, String description) {
        this(command, description, true);
    }

    CommandType(String command, String description, boolean showInHelp) {
        this.command = command;
        this.description = description;
        this.showInHelp = showInHelp;
    }

    public String getCommand() {
        return this.command;
    }

    public String getDescription() {
        return this.description;
    }

    public boolean isShowInHelp() {
        return this.showInHelp;
    }

    public boolean isUnknown() {
        return this == UNKNOWN;
    }

    public static CommandType parse(String command) {
        try {
            for (CommandType commandType : values()) {
                if (commandType.getCommand().equals(command)) {
                    return commandType;
                }
            }
        } catch (IllegalArgumentException ignored) {
        }
        return UNKNOWN;
    }

    public static String toHelp() {
        return String.format("Commands:\n%s", Arrays.stream(values())
                                                        .filter(CommandType::isShowInHelp)
                                                        .map(commandType -> String.format("\t%s - %s", commandType.getCommand(), commandType.getDescription()))
                                                        .collect(Collectors.joining("\n"))
        );
    }

}
