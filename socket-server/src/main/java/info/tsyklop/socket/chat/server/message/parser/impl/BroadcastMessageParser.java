package info.tsyklop.socket.chat.server.message.parser.impl;

import info.tsyklop.socket.chat.server.client.ClientThread;
import info.tsyklop.socket.chat.server.exception.MessageException;
import info.tsyklop.socket.chat.server.message.Message;
import info.tsyklop.socket.chat.server.message.impl.BroadcastMessage;
import info.tsyklop.socket.chat.server.message.parser.AbstractParser;
import info.tsyklop.socket.chat.server.message.parser.MessageParser;

import java.util.Objects;

public class BroadcastMessageParser extends AbstractParser {

    @Override
    public boolean test(String message) {
        return !isCommandMessage(message) && !isPersonalMessage(message) && !isDisconnectMessage(message);
    }

    @Override
    public Message parse(ClientThread source, String message) throws MessageException {
        return new BroadcastMessage(source, message);
    }

}
