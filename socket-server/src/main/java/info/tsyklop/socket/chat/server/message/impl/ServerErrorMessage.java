package info.tsyklop.socket.chat.server.message.impl;

import info.tsyklop.socket.chat.server.message.AbstractMessage;
import info.tsyklop.socket.chat.server.message.Message;
import info.tsyklop.socket.chat.server.model.MessageType;

public class ServerErrorMessage extends AbstractMessage {

    public ServerErrorMessage(String message) {
        super(null, MessageType.ERROR, message);
    }

    @Override
    public String toString() {
        return String.format("<Server>: Error - %s", getMessage());
    }

    public static Message create(String message) {
        return new ServerErrorMessage(message);
    }

}
