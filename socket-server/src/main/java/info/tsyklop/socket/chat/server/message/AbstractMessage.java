package info.tsyklop.socket.chat.server.message;

import info.tsyklop.socket.chat.server.client.ClientThread;
import info.tsyklop.socket.chat.server.model.MessageType;

public abstract class AbstractMessage implements Message {

    private final String message;

    private final MessageType type;

    private final ClientThread owner;

    protected AbstractMessage(ClientThread owner, MessageType type, String message) {
        this.type = type;
        this.message = message;
        this.owner = owner;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public MessageType getType() {
        return this.type;
    }

    @Override
    public ClientThread getOwner() {
        return this.owner;
    }

}
