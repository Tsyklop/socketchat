package info.tsyklop.socket.chat.server.message.impl;

import info.tsyklop.socket.chat.server.client.ClientThread;
import info.tsyklop.socket.chat.server.message.AbstractMessage;
import info.tsyklop.socket.chat.server.model.MessageType;

import java.util.Objects;

public class BroadcastMessage extends AbstractMessage {

    public BroadcastMessage(ClientThread owner, String message) {
        super(owner, MessageType.BROADCAST, message);
    }

    @Override
    public String toString() {
        return String.format("[ALL]<%s> - %s", Objects.nonNull(getOwner()) ? getOwner().getName() : "Server", getMessage());
    }

    public static BroadcastMessage create(String message) {
        return create(null, message);
    }

    public static BroadcastMessage create(ClientThread owner, String message) {
        return new BroadcastMessage(owner, message);
    }

}
