package info.tsyklop.socket.chat.server.repository;

import info.tsyklop.socket.chat.server.client.ClientThread;

import java.util.*;
import java.util.stream.Collectors;

public class ClientRepository {

    private static final Map<String, ClientThread> CONNECTED_CLIENTS = new HashMap<>();

    private static final List<ClientThread> NON_CONNECTED_CLIENTS = Collections.synchronizedList(new ArrayList<>());
    
    public synchronized static Set<String> getConnectedUsers() {
        return CONNECTED_CLIENTS.keySet();
    }

    public synchronized static String getConnectedUsersAsString() {
        return CONNECTED_CLIENTS.keySet().stream().map(s -> "@" + s).collect(Collectors.joining(","));
    }

    public synchronized static void addNonConnectedClient(ClientThread clientThread) {
        NON_CONNECTED_CLIENTS.add(clientThread);
    }

    public synchronized static boolean addConnectedClient(String name, ClientThread clientThread) {
        if(NON_CONNECTED_CLIENTS.remove(clientThread)) {
            CONNECTED_CLIENTS.put(name, clientThread);
            return true;
        }
        return false;
    }

    public synchronized static Collection<ClientThread> getConnectedClients() {
        return CONNECTED_CLIENTS.values();
    }

    public synchronized static ClientThread findConnectedClientByName(String clientName) {
        return CONNECTED_CLIENTS.get(clientName);
    }

    public synchronized static void removeClient(String name, ClientThread clientThread) {
        CONNECTED_CLIENTS.remove(name);
        NON_CONNECTED_CLIENTS.remove(clientThread);
    }

    public synchronized static boolean containsClientByName(String name) {
        return CONNECTED_CLIENTS.containsKey(name);
    }

}
