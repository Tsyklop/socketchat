package info.tsyklop.socket.chat.server.config;

public class Config {

    private int port = 8080;

    public Config(String[] args) {

        for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            switch (arg) {
                case "-p":
                    try {
                        this.port = Integer.parseInt(args[i + 1]);
                    } catch (NumberFormatException ignored) {
                    }
                    break;
            }
        }

    }

    public int getPort() {
        return this.port;
    }

}
