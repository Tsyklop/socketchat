package info.tsyklop.socket.chat.server.message;

import info.tsyklop.socket.chat.server.client.ClientThread;
import info.tsyklop.socket.chat.server.model.MessageType;

public interface Message {

    String getMessage();

    MessageType getType();

    ClientThread getOwner();

}
