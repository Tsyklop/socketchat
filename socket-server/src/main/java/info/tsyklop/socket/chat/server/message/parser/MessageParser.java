package info.tsyklop.socket.chat.server.message.parser;

import info.tsyklop.socket.chat.server.client.ClientThread;
import info.tsyklop.socket.chat.server.exception.MessageException;
import info.tsyklop.socket.chat.server.message.Message;

public interface MessageParser {

    boolean test(String message);

    Message parse(ClientThread source, String message) throws MessageException;

}
